from distutils.core import setup

setup(
        name='ProcessRunner',
        version='0.1',
        packages=['ProcessRunner'],
        url='',
        license='Modified BSD License',
        author='Brian Larsen',
        author_email='balarsen@lanl.gov',
        description='Package to facilitate the running of many command line programs in parallel'
)
