import datetime
import os
import shutil
import subprocess
import sys
import tempfile
import time
from functools import total_ordering
from multiprocessing import cpu_count

try:
    import tqdm

    TQDM = True
except ImportError:
    TQDM = False

try:
    assert (sys.version_info.major >= 3)
    assert (sys.version_info.minor >= 3)
except (AssertionError, AttributeError):
    class TimeoutError(Exception):
        """Raised when a time-limited process times out"""
        pass

__all__ = ['ProcessException', 'Process', 'MultiRunner']


class ProcessException(Exception):
    """Class for errors in running processes"""
    pass


def _mk_tempdir(suffix='_ProcessRunner', dirname=None):
    """
    create a secure temp directory, wrapper around :py:class:`tempfile.mkdtemp`

    Other Parameters
    ----------------
    suffix : str
        String to add on the end of the created temporary directories (default: '_ProcessRunner')

    Returns
    -------
    str
        name of a temp directory

    Examples
    --------
    >>> import ProcessRunner
    >>> td = ProcessRunner._mk_tempdir()
    >>> print(td)
    /var/folders/5r/3c2nfrr12vz53wrg2tmps2r80000gp/T/tmpzihqvrm0_ProcessRunner
    >>> type(td)
    str
    :param dirname:
    :type dirname:
    :type suffix: object
    """
    tempdir = tempfile.mkdtemp(dir=dirname, suffix=suffix)
    return tempdir


def _rm_tempdir(tempdir):
    """
    remove the temp directory, wrapper around :py:class:`shutil.rmtree`

    Parameters
    ----------
    tempdir : str

    Examples
    --------
    >>> import os
    >>> import ProcessRunner
    >>> td = ProcessRunner._mk_tempdir()
    >>> print(td, os.path.isdir(td))
    (/var/folders/5r/3c2nfrr12vz53wrg2tmps2r80000gp/T/tmpzihqvrm0_ProcessRunner, True)
    >>> ProcessRunner._rm_tempdir(td)
    >>> print(os.path.isdir(td))
    False
    """
    assert isinstance(tempdir, str)
    assert os.path.isdir(tempdir), "Directory did not exist"
    shutil.rmtree(tempdir)


@total_ordering
class Process(object):
    """
    Class that represents a process to be run, this provides the basis of ProcessRunner

    Parameters
    ----------
    command : str
        The command to run

    Other Parameters
    ----------------
    pargs : list
        List of positional arguments
    kwargs : list
        List of keyword arguments
    shell : bool
        Should the command be run in a shell (subprocess.Popen with shell=shell)
    ableToRun : bool
        Should the command be allowed to run, False means process will not be run
    timeout : float
        Number of seconds a process has to finish before being killed raising a TimeoutError

    Examples
    --------
    >>> import time
    >>> import ProcessRunner
    >>> p = ProcessRunner.Process.from_string('python -c "print(['R0'])"')
    >>> p.run(verbose=True, nice=4)
    >>> time.sleep(2) # just so we are sure it is done
    >>> print(p.finished()) # a number means this is done
    0
    >>> print(p.prettylog) # times in the log are UTC
    # python  -c "print(['R0'])"
    # 2016-04-13 23:38:33.262008
    # ------------------------------------------------------------------------------
    # ------------------------------------------------------------------------------
    # 2016-04-13T23:38:36.543912  --  3.2816410064697266 seconds  --  0 retcode
    """

    def __init__(self, command, pargs=None, kwargs=None,
                 shell=False,
                 ableToRun=True,
                 timeout=None,
                 finishTrigger=None):
        self.tfp = None
        self.time = None
        self.proc = None
        self.success = None
        self.log = None
        self.stderr = None
        self.stdout = None
        self.tfp_stdout = None
        self.tfp_stderr = None
        self.command = command
        self.pargs = pargs
        self.kwargs = kwargs
        self.shell = shell
        self.finishTrigger = finishTrigger
        assert isinstance(self.shell, bool)
        self.ableToRun = ableToRun
        assert isinstance(self.ableToRun, bool)
        self.timeout = timeout
        assert (self.timeout is None or float(self.timeout))

    def __hash__(self):
        return hash(self.str_command_line)

    def __str__(self):
        if self.pargs:
            l_pargs = len(self.pargs)
        else:
            l_pargs = 0
        if self.kwargs:
            l_kwargs = len(self.kwargs)
        else:
            l_kwargs = 0
        return 'Process({0}: {1}-args {2}-kwargs)'.format(self.command,
                                                          l_pargs,
                                                          l_kwargs)

    def __lt__(self, other):
        return self.str_command_line < other.str_command_line

    __repr__ = __str__

    def __del__(self):
        """
        attempt some cleanup
        """
        # if the process is running kill it
        try:
            if self.proc.poll() is None:
                self.proc.terminate()
            if self.proc.poll() is None:
                self.proc.kill()
        except (AttributeError, ProcessException):
            pass
        # if the file exists close it so it deletes
        try:
            self.tfp.close()
        except AttributeError:
            pass
        # if the file exists close it so it deletes
        try:
            self.tfp_stdout.close()
        except AttributeError:
            pass
        # if the file exists close it so it deletes
        try:
            self.tfp_stderr.close()
        except AttributeError:
            pass

    def __eq__(self, other):
        return (self.command == other.command and
                self.kwargs == other.kwargs and
                self.pargs == other.pargs)

    @property
    def prettylog(self):
        """
        Format the stored log nicely for display

        Returns
        -------
        str
            Pretty string representation of the output of the process
        """
        return ''.join(self.log)

    @property
    def str_command_line(self):
        """
        Make the stored command set into a string, normally easier to read

        Returns
        -------
        str
            The command line as a string
        """
        if hasattr(self.kwargs, '__iter__') and hasattr(self.pargs, '__iter__'):
            return "{0} {1} {2}".format(self.command, ' '.join(self.kwargs), ' '.join(self.pargs))
        elif hasattr(self.kwargs, '__iter__'):
            return "{0} {1}".format(self.command, ' '.join(self.kwargs))
        elif hasattr(self.pargs, '__iter__'):
            return "{0} {1}".format(self.command, ' '.join(self.pargs))
        else:
            return "{0}".format(self.command)

    @property
    def list_command_line(self):
        """
        The list that is the full command as understood by :py:class:`subprocess.Popen`

        Returns
        -------
        list
            The command line as a list
        """
        cl = [self.command]
        if self.kwargs:
            cl.extend(self.kwargs)
        if self.pargs:
            cl.extend(self.pargs)
        return cl

    @classmethod
    def from_string(cls, instr):
        """
        Create a new :mod:`~Process` from a string command line, very useful for the creation of easy commands

        Parameters
        ----------
        instr : str
            The command line to change into a :mod:`~Process`

        Returns
        -------
        Process
            :mod:`~Process` instance created from the string passed in
        """
        assert isinstance(instr, str)
        tmp = instr.split()
        command = tmp[0]
        kwargs = [v for v in tmp if '=' in v]
        pargs = [v for v in tmp[1:] if v not in kwargs]
        return cls(command, pargs, kwargs)

    def run(self, nice=None, suffix='_ProcessRunner', verbose=False):
        """
        Run the code pointed to by the Process object

        Other Parameters
        ----------------
        nice : int
            Priority to set on processes run by the :mod:`~Process`
        suffix : str
            Suffix to append to the end of the temp directory where the code is run
        verbose : bool
            Flag on if the Process should print information on starting and stopping

        Returns
        -------
        None
        """
        assert (nice is None or isinstance(nice, int) or nice.is_integer())
        assert isinstance(suffix, str)
        assert isinstance(verbose, bool)
        if not self.ableToRun:
            return
        if nice is not None:
            nice = int(nice)
        self.tfp_stdout = tempfile.NamedTemporaryFile(suffix=suffix, mode='wt')
        self.tfp_stderr = tempfile.NamedTemporaryFile(suffix=suffix, mode='wt')
        self.tfp = tempfile.NamedTemporaryFile(suffix=suffix, mode='wt')
        self.tfp.file.write('# {0}\n'.format(self.str_command_line))
        self.tfp.file.write('# {0}\n'.format(datetime.datetime.utcnow()))
        self.tfp.file.write('# {0}\n'.format('-' * 78))
        self.tfp.file.flush()
        self.tfp_stdout.file.flush()
        self.tfp_stderr.file.flush()
        self.time = time.time()
        run_command = self.list_command_line
        if nice is not None:
            run_command.insert(0, '{0}'.format(nice))
            run_command.insert(0, '-n')
            run_command.insert(0, 'nice')
        if verbose:
            print('{0} : starting {1}'.format(datetime.datetime.utcnow().isoformat(),
                                              run_command))
        self.proc = subprocess.Popen(run_command,
                                     stdout=self.tfp_stdout,
                                     stderr=self.tfp_stderr,
                                     shell=self.shell)

    def finished(self):
        """
        Method to test if a process has finished running

        Returns
        -------
        NotDone : None
            Returns None is the process is not finished
        Done : int
            Returns the return code from the process on competition
        """
        # if things were never run this will be None
        if self.proc is None:
            raise ProcessException('Process not run')
        if self.proc.poll() is None:
            if self.timeout is not None:
                tnow = time.time()
                if (tnow - self.time) > self.timeout:
                    self.proc.terminate()
                    if self.proc.poll() is None:
                        self.proc.kill()
                    raise (TimeoutError("Timed out after {0:.1f} seconds".format(tnow - self.time)))
            return None  # not done
        # if it is done then we just check returncode
        elif self.proc.returncode != 0:  # failed
            self.success = False
        else:
            self.success = True
        # if the process is already done then the log file will be gone
        if not os.path.isfile(self.tfp.name):
            return self.proc.returncode
        # finish and close the file
        self.tfp.file.flush()
        self.tfp_stdout.file.flush()
        self.tfp_stderr.file.flush()
        # Populate self.stdout and self.stderr
        # put the stdout and stderr into the logfile
        #
        with open(self.tfp_stdout.name, 'rt') as fp:
            self.stdout = fp.readlines()
            self.tfp.file.writelines(self.stdout)
        with open(self.tfp_stderr.name, 'rt') as fp:
            self.stderr = fp.readlines()
            self.tfp.file.writelines(fp.readlines())

        self.tfp.file.write('# {0}\n'.format('-' * 78))
        self.tfp.file.write('# {0}  --  {1} seconds  --  {2} retcode\n'.format(datetime.datetime.utcnow().isoformat(),
                                                                               time.time() - self.time,
                                                                               self.proc.returncode))
        self.tfp.file.flush()
        with open(self.tfp.name, 'rt') as fp:
            self.log = fp.readlines()
        self.tfp.close()
        
        if callable(self.finishTrigger):
            self.finishTrigger(self)
        return self.proc.returncode

    def savelog(self, filename, overwrite=False):
        """
        Save the log that the Process saved from stdio and stderr to a file

        Parameters
        ----------
        filename : str
            Filename to save the Process stdio and stderr to

        Other Parameters
        ----------------
        overwrite : bool
            Allow overwrite of the log file

        Returns
        -------
        None
        :param overwrite:

        """
        assert isinstance(filename, str)
        assert isinstance(overwrite, bool)
        if self.log is None:
            raise ProcessException('No log to save, was the process run?')
        if os.path.exists(filename):
            if not overwrite:
                raise ValueError('File {0} exists and will not be overwritten, try overwrite=True'.format(filename))
            else:
                os.remove(filename)
        with open(filename, 'wt') as fp:
            fp.writelines(self.log)


class MultiRunner(object):
    """
    class takes in a list of Process objects and runs them N at a time

    Parameters
    ----------
    procs : iterable of ProcessRunner.Process
        Iterable (typically list or tuple) of ProcessRunner.Process instances


    Other Parameters
    ----------------
    maxprocs : int
        Number of simultaneous processes to run (Default: number of CPUs-1 or 1)

    Examples
    ========
    >>> import ProcessRunner
    >>> procs = [
            ProcessRunner.Process.from_string('python -c "print([R0])"'),
            ProcessRunner.Process.from_string('python -c "print([R1])"'),
            ProcessRunner.Process.from_string('python -c "print([R2])"'),
            ProcessRunner.Process.from_string('python -c "print([R3])"'),
            ProcessRunner.Process.from_string('python -c "print([R4])"'),
            ProcessRunner.Process.from_string('python -c "print([R5])"'),
            ProcessRunner.Process.from_string('python -c "print([R6])"'),
            ProcessRunner.Process.from_string('python -c "print([R7])"'),
            ProcessRunner.Process.from_string('python -c "print([R8])"'),
            ProcessRunner.Process.from_string('python -c "print([R9])"'),
            ]
    >>> mr1 = ProcessRunner.MultiRunner(procs, 2)
    >>> mr1.run(verbose=True, nice=4)
    Popped Process(python: 2-args 0-kwargs), about to start it
    Popped Process(python: 2-args 0-kwargs), about to start it
    Process(python: 2-args 0-kwargs) Finished: retcode 0
    Popped Process(python: 2-args 0-kwargs), about to start it
    Process(python: 2-args 0-kwargs) Finished: retcode 0
    Popped Process(python: 2-args 0-kwargs), about to start it
    Process(python: 2-args 0-kwargs) Finished: retcode 0
    Popped Process(python: 2-args 0-kwargs), about to start it
    Process(python: 2-args 0-kwargs) Finished: retcode 0
    Popped Process(python: 2-args 0-kwargs), about to start it
    Process(python: 2-args 0-kwargs) Finished: retcode 0
    Popped Process(python: 2-args 0-kwargs), about to start it
    Process(python: 2-args 0-kwargs) Finished: retcode 0
    Popped Process(python: 2-args 0-kwargs), about to start it
    Process(python: 2-args 0-kwargs) Finished: retcode 0
    Popped Process(python: 2-args 0-kwargs), about to start it
    Process(python: 2-args 0-kwargs) Finished: retcode 0
    Popped Process(python: 2-args 0-kwargs), about to start it
    Process(python: 2-args 0-kwargs) Finished: retcode 0
    Process(python: 2-args 0-kwargs) Finished: retcode 0

    """

    def __init__(self, procs, maxprocs=None, progress=False):
        """

        :type procs: list
        """
        assert isinstance(procs, (list, tuple))
        assert (maxprocs is None or isinstance(maxprocs, int) or maxprocs.is_integer())
        assert isinstance(progress, bool)
        self.output = []  # output processes
        self.ngood = 0  # number of processes that returned 0
        self.nbad = 0  # number of processes that returned != 0
        self.procs = procs
        self.progress = progress
        if maxprocs is None:
            if cpu_count() == 1:
                self.maxprocs = 1
            else:
                self.maxprocs = cpu_count() - 1
        else:
            self.maxprocs = maxprocs

    def __str__(self):
        s = ""
        s += "MultiRunner:"
        for v in self:
            s += "\n    {0}".format(v)
        return s

    def __repr__(self):
        return "MultiRunner with {0} processes".format(len(self))

    def run(self, resolution=0.5, verbose=False, nice=None):
        """
        run the processes maxproc at a time

        Other Parameters
        ----------------
        resolution : float
            Time resolution to check is processes are finished (default: 0.5s)
        verbose : bool
            Print information about starting and finishing processes (default: False)
        nice : int
            Priority to set on processes run by the MultiRunner
        """
        assert float(resolution) and float(resolution) > 0
        assert isinstance(verbose, bool)
        assert (nice is None or isinstance(nice, int) or nice.is_integer())

        running = {}  # dict with the key as the Popen object containing a list of command line and start time

        # outer while if there are things running or need to run
        if self.progress and TQDM:
            tq = tqdm.tqdm(total=len(self), initial=0, leave=False)

        while running or self.procs:
            # new while if there are less than self.maxprocs running (or none left)
            while (len(running) < self.maxprocs) and self.procs:
                proc = self.procs.pop(0)
                if verbose: print("Popped {0}, about to start it".format(proc))
                running[proc] = proc.run(nice=nice)
            # now that they are all running see if any are done

            for proc in list(running.keys()):
                # if verbose: print("Testing if {0} is done".format(proc))
                if proc.finished() is None:  # still running
                    # if verbose: print("{0}    It is not")
                    continue
                # OK process done, did it succeed?
                if verbose: print("{0} Finished: retcode {1}".format(proc, proc.proc.returncode))
                if proc.success:
                    self.ngood += 1
                else:
                    self.nbad += 1
                self.output.append(proc)
                del running[proc]
                if self.progress and TQDM:
                    tq.update()
            time.sleep(resolution)
