

ProcessRunner
=============

.. automodule:: ProcessRunner

.. currentmodule:: ProcessRunner

.. rubric:: Classes


.. autosummary::
    :toctree: autosummary

    Process
    MultiRunner


.. rubric:: Exceptions
.. autosummary::

    ProcessException


.. automodule:: ProcessRunner
    :members:
    :undoc-members:
    :show-inheritance:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

