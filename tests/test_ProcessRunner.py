import os
import shutil
import tempfile
import time
import unittest
from multiprocessing import cpu_count
from unittest import TestCase

from ProcessRunner import _mk_tempdir, _rm_tempdir, Process, ProcessException, MultiRunner

# For python2 compat
try:
    TimeoutError
except NameError:
    from ProcessRunner import TimeoutError

try:
    FileNotFoundError
except NameError:
    FileNotFoundError = OSError

class Test_Process(TestCase):
    def setUp(self):
        super(Test_Process, self).setUp()
        self.command = 'command'
        self.pargs = ['arg1', 'arg2', 'output']
        self.kwargs = ['--test=1']
        self.process = Process(command=self.command,
                               pargs=self.pargs,
                               kwargs=self.kwargs)

    def test_lt(self):
        p1 = Process(command='aCommand')
        p2 = Process(command='bCommand')
        self.assertTrue(p1 < p2)
        self.assertFalse(p1 > p2)

    def test_gt(self):
        p1 = Process(command='aCommand')
        p2 = Process(command='bCommand')
        self.assertTrue(p2 > p1)
        self.assertFalse(p2 < p1)

    def test_Process(self):
        self.assertEqual(self.command, self.process.command)
        self.assertEqual(self.pargs, self.process.pargs)
        self.assertEqual(self.kwargs, self.kwargs)

    def test_str_command_line1(self):
        self.assertEqual('command --test=1 arg1 arg2 output', self.process.str_command_line)

    def test_str_command_line2(self):
        p = Process(command=self.command,
                    pargs=self.pargs)
        self.assertEqual('command arg1 arg2 output', p.str_command_line)

    def test_str_command_line3(self):
        p = Process(command=self.command,
                    pargs=self.kwargs)
        self.assertEqual('command --test=1', p.str_command_line)

    def test_str_command_line4(self):
        p = Process(command=self.command)
        self.assertEqual('command', p.str_command_line)

    def test_list_command_line(self):
        self.assertEqual(['command', '--test=1', 'arg1', 'arg2', 'output'],
                         self.process.list_command_line)

    def test_eq(self):
        self.assertEqual(self.process, Process.from_string('command --test=1 arg1 arg2 output'))
        self.assertNotEqual(self.process, Process.from_string('command --test=1 arg15 arg2 output'))

    def test_from_string(self):
        proc = Process.from_string('command --test=1 arg1 arg2 output')
        self.assertEqual(self.process, proc)

    def test_str(self):
        self.assertEqual('Process(command: 3-args 1-kwargs)', str(self.process))


class Test_Process_w_run(TestCase):
    def test_del_early(self):
        p = Process.from_string('sleep 100')
        p.run()
        self.assertTrue(p.finished() is None)

    def test_run(self):
        p = Process.from_string('python -c "print([1,2,3])"')
        p.run()
        while p.finished() is None:
            pass
        self.assertEqual(0, p.finished())
        self.assertEqual(5, len(p.log))
        self.assertTrue('0 retcode' in p.log[-1])
        self.assertTrue(p.success)

    def test_run_not_run(self):
        p = Process.from_string('python -c "print([1,2,3])"')
        self.assertRaises(ProcessException, p.finished)

    def test_run_timeout(self):
        p = Process.from_string('sleep 10')
        p.timeout = 0.1
        p.run()
        time.sleep(0.1)
        self.assertRaises(TimeoutError, p.finished)
        self.assertTrue(p.proc.returncode is None)

    def test_run_fail(self):
        p = Process.from_string('python -c "import sys; sys.exit(1)')
        p.run()
        while p.finished() is None:
            pass
        self.assertEqual(1, p.finished())
        self.assertFalse(p.success)

    def test_run_nice(self):
        p = Process.from_string('python -c "print([1,2,3])"')
        p.run(nice=4)
        while p.finished() is None:
            pass
        self.assertEqual(0, p.finished())
        self.assertEqual(5, len(p.log))
        self.assertTrue('0 retcode' in p.log[-1])
        self.assertTrue(p.success)

    def test_savelog(self):
        tfp = tempfile.NamedTemporaryFile(delete=False)
        tfp.close()
        try:
            p = Process.from_string('ls {0}'.format(os.path.dirname(__file__)))
            self.assertRaises(ProcessException, p.savelog, 'nomane')
            p.run(nice=4)
            while p.finished() is None:
                pass
            self.assertRaises(ValueError, p.savelog, tfp.name)
            p.savelog(tfp.name, overwrite=True)
            with open(tfp.name, 'rt') as fp:
                dat = fp.readlines()
            self.assertEqual(p.log, dat)
        finally:
            os.remove(tfp.name)

    def test_prettylog(self):
        p = Process.from_string('python -c "print([1,2,3])"')
        p.run(nice=4)
        while p.finished() is None:
            pass
        self.assertEqual(0, p.finished())
        self.assertIn('# python  -c "print([1,2,3])"', p.prettylog)
        self.assertEqual('0 retcode', p.prettylog[-10:-1])
        self.assertTrue(p.success)

    def trigger(self, p):
        self.triggered = True

        # Make sure p got passed correctly
        self.assertEqual(5, len(p.log))

    def test_run_trigger(self):
        self.triggered = False
        p = Process("python", kwargs=["-c", '"print([1,2,3])"'], finishTrigger=self.trigger)
        p.run()
        while p.finished() is None:
            pass
        self.assertEqual(0, p.finished())
        self.assertEqual(5, len(p.log))
        self.assertTrue('0 retcode' in p.log[-1])
        self.assertTrue(p.success)

        # Make sure self.trigger() was ran
        self.assertTrue(self.triggered)

class Test_tempdir(TestCase):
    def test_mk_tempdir(self):
        try:
            td = _mk_tempdir(suffix="_ProcessRunnerTest")
            self.assertTrue(os.path.isdir(td))
        finally:
            shutil.rmtree(td)

    def test_mk_tempdir2(self):
        try:
            td = _mk_tempdir(dirname=os.path.dirname(__file__), suffix="_ProcessRunnerTest")
            self.assertTrue(os.path.isdir(td))
            self.assertTrue(os.path.dirname(td) == os.path.dirname(__file__))
        finally:
            shutil.rmtree(td)

    def test_rm_tempdir(self):
        td = tempfile.mkdtemp()
        try:
            self.assertTrue(os.path.isdir(td))
            _rm_tempdir(td)
            self.assertFalse(os.path.isdir(td))
        finally:
            try:
                shutil.rmtree(td)
            except FileNotFoundError:
                pass


class Test_MultiRunner(TestCase):
    def setUp(self):
        super(Test_MultiRunner, self).setUp()
        self.procs = [
            Process.from_string('python -c "print([R0])"'),
            Process.from_string('python -c "print([R1])"'),
            Process.from_string('python -c "print([R2])"'),
            Process.from_string('python -c "print([R3])"'),
            Process.from_string('python -c "print([R4])"'),
            Process.from_string('python -c "print([R5])"'),
            Process.from_string('python -c "print([R6])"'),
            Process.from_string('python -c "print([R7])"'),
            Process.from_string('python -c "print([R8])"'),
            Process.from_string('python -c "print([R9])"'),
        ]

    def tearDown(self):
        super(Test_MultiRunner, self).tearDown()
        del self.procs

    def test_maxprocs(self):
        mr = MultiRunner([], 3)
        self.assertEqual(mr.maxprocs, 3)
        mr = MultiRunner([])
        if cpu_count() > 1:
            self.assertEqual(cpu_count() - 1, mr.maxprocs)
        else:
            self.assertEqual(1, mr.maxprocs)

    def test_run1(self):
        mr = MultiRunner(self.procs, 2)
        mr.run(verbose=False)
        self.assertEqual(10, len(mr.output))
        mr.output = sorted(mr.output)  # they are sortable
        for ii, m in enumerate(mr.output):
            self.assertEqual(0, m.proc.returncode)
            self.assertEqual(5, len(m.log))
            self.assertTrue('R{0}'.format(ii) in m.log[0])

    def test_run2(self):
        del self.procs[3:]
        p = Process.from_string('python -c "import sys; sys.exit(1)"')
        self.procs.append(p)
        mr1 = MultiRunner(self.procs, 2)
        mr1.run(verbose=False)
        self.assertEqual(4, len(mr1.output))
        mr1.output = sorted(mr1.output)  # they are sortable
        self.assertEqual(1, mr1.output[0].proc.returncode)
        self.assertEqual(0, mr1.output[1].proc.returncode)
        self.assertEqual(0, mr1.output[2].proc.returncode)
        self.assertEqual(0, mr1.output[3].proc.returncode)
        self.assertEqual(1, mr1.nbad)
        self.assertEqual(3, mr1.ngood)

    def test_run3(self):
        del self.procs[3:]
        p = Process.from_string('python -c "import sys; sys.exit(1)"')
        self.procs.append(p)
        mr1 = MultiRunner(self.procs, 2)
        mr1.run(verbose=False, nice=4)
        self.assertEqual(4, len(mr1.output))
        mr1.output = sorted(mr1.output)  # they are sortable
        self.assertEqual(1, mr1.output[0].proc.returncode)
        self.assertEqual(0, mr1.output[1].proc.returncode)
        self.assertEqual(0, mr1.output[2].proc.returncode)
        self.assertEqual(0, mr1.output[3].proc.returncode)
        self.assertEqual(1, mr1.nbad)
        self.assertEqual(3, mr1.ngood)


if __name__ == "__main__":
    unittest.main()
