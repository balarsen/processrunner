[![build status](https://git.lanl.gov/balarsen/ProcessRunner/badges/master/build.svg)](https://git.lanl.gov/balarsen/ProcessRunner/commits/master)
[![coverage report](https://git.lanl.gov/balarsen/ProcessRunner/badges/master/coverage.svg)](https://git.lanl.gov/balarsen/ProcessRunner/commits/master)

	 ______                                 ______                                
	(_____ \                               (_____ \                               
	 _____) )___ ___   ____ _____  ___  ___ _____) )_   _ ____  ____  _____  ____ 
	|  ____/ ___) _ \ / ___) ___ |/___)/___)  __  /| | | |  _ \|  _ \| ___ |/ ___)
	| |   | |  | |_| ( (___| ____|___ |___ | |  \ \| |_| | | | | | | | ____| |    
	|_|   |_|   \___/ \____)_____|___/(___/|_|   |_|____/|_| |_|_| |_|_____)_|    
	                                                                              

# README #

ProcessRunner was created out of a need to run many command line programs in parallel from a long list always keeping N running at a given time keeping track of screen output and return code. It is a straightforward wrapper around Python's `subprocess` module. It is compatible with python2.6+ and python 3.4+. 


## Installation ##
### Just the code ###
1. There are no dependencies outside of Python standard library.
2. In the directory containing setup.py simply type `python setup.py install --user`

### Build the documentation ###
1. Dependencies are:
    1. [sphinx](http://www.sphinx-doc.org/en/stable/)
    2. [numpydoc](https://pypi.python.org/pypi/numpydoc)
    3. [sphinx\_rtd\_theme](https://github.com/snide/sphinx_rtd_theme)
    4. make
2. Install ProcessRunner
3. cd into sphinx directory
4. Run the command `make html` (or other sphinx supported format)
5. Output is then in build/html

# Examples #
## Run a single job ##

~~~
>>> import time
>>> import ProcessRunner
>>> p = ProcessRunner.Process.from_string('python -c "print(['R0'])"')
>>> p.run(verbose=True, nice=4)
>>> time.sleep(2) # just so we are sure it is done
>>> print(p.finished()) # a number means this is done
0
>>> print(p.prettylog) # times in the log are UTC
# python  -c "print(['R0'])"
# 2016-04-13 23:38:33.262008
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# 2016-04-13T23:38:36.543912  --  3.2816410064697266 seconds  --  0 retcode
~~~

## Run a collection of jobs ##

~~~
>>> import ProcessRunner
>>> procs = [
        ProcessRunner.Process.from_string('python -c "print([R0])"'),
        ProcessRunner.Process.from_string('python -c "print([R1])"'),
        ProcessRunner.Process.from_string('python -c "print([R2])"'),
        ProcessRunner.Process.from_string('python -c "print([R3])"'),
        ProcessRunner.Process.from_string('python -c "print([R4])"'),
        ProcessRunner.Process.from_string('python -c "print([R5])"'),
        ProcessRunner.Process.from_string('python -c "print([R6])"'),
        ProcessRunner.Process.from_string('python -c "print([R7])"'),
        ProcessRunner.Process.from_string('python -c "print([R8])"'),
        ProcessRunner.Process.from_string('python -c "print([R9])"'),
        ]
>>> mr1 = ProcessRunner.MultiRunner(procs, 2)
>>> mr1.run(verbose=False, nice=4)
Popped Process(python: 2-args 0-kwargs), about to start it
Popped Process(python: 2-args 0-kwargs), about to start it
Process(python: 2-args 0-kwargs) Finished: retcode 0
Popped Process(python: 2-args 0-kwargs), about to start it
Process(python: 2-args 0-kwargs) Finished: retcode 0
Popped Process(python: 2-args 0-kwargs), about to start it
Process(python: 2-args 0-kwargs) Finished: retcode 0
Popped Process(python: 2-args 0-kwargs), about to start it
Process(python: 2-args 0-kwargs) Finished: retcode 0
Popped Process(python: 2-args 0-kwargs), about to start it
Process(python: 2-args 0-kwargs) Finished: retcode 0
Popped Process(python: 2-args 0-kwargs), about to start it
Process(python: 2-args 0-kwargs) Finished: retcode 0
Popped Process(python: 2-args 0-kwargs), about to start it
Process(python: 2-args 0-kwargs) Finished: retcode 0
Popped Process(python: 2-args 0-kwargs), about to start it
Process(python: 2-args 0-kwargs) Finished: retcode 0
Popped Process(python: 2-args 0-kwargs), about to start it
Process(python: 2-args 0-kwargs) Finished: retcode 0
Process(python: 2-args 0-kwargs) Finished: retcode 0
~~~

# Quick API #
See the sphinx docs for a complete API reference.

~~~
ProcessRunner.Process

Init signature: ProcessRunner.Process(self, command, pargs=None, kwargs=None, shell=False, ableToRun=True, timeout=None)

Docstring:
Class that represents a process to be run, this provides the basis of ProcessRunner

Parameters
----------
command : str
    The command to run

Other Parameters
----------------
pargs : list
    List of positional arguments
kwargs : list
    List of keyword arguments
shell : bool
    Should the command be run in a shell (subprocess.Popen with shell=shell)
ableToRun : bool
    Should the command be allowed to run, False means process will not be run
timeout : float
    Number of seconds a process has to finish before being killed raising a TimeoutError 
~~~

~~~
ProcessRunner.MultiRunner

Init signature: ProcessRunner.MultiRunner(self, procs, maxprocs=None)

Docstring:
class takes in a list of Process objects and runs them N at a time

Parameters
----------
procs : iterable of ProcessRunner.Process
    Iterable (typically list or tuple) of ProcessRunner.Process instances


Other Parameters
----------------
maxprocs : int
    Number of simultaneous processes to run (Default: number of CPUs-1 or 1)
~~~

### Contribution guidelines ###

I welcome any updates or modifications that make this more functional.


### Who do I talk to? ###

Brian Larsen, Los Alamos National Laboratory, balarsen@lanl.gov

### Wishlist ###
Things that would be great to have to enhance this code:

* Some kind of shell (bash) front end to be able to more directly call this as a script by itself
	* Not immediately obvious to me how to pass in a list of command lines to do this
* Ability for a progress bar hook on commands being run	


### License ###
Copyright © 2016, Los Alamos National Security, LLC
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Los Alamos National Security, LLC nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
